;#SingleInstance force ;只能启动一个ahk程序实例，防止重复启动
#Persistent
#NoEnv

;暗黑3/恶魔猎手/暗影三刀/AHK脚本
;##################################
;说明：
;-----MacroID = 0代表宏已停止启动，MacroID = 3代表宏已启动
;默认按键：
;-----鼠标左键 = 战宠
;-----鼠标右键 = 刀扇
;-----键盘A = 影轮翻
;-----键盘S = 暗影之力
;-----键盘D = 暗影飞刀
;-----键盘F = 复仇
;-----键盘W = 强制移动
;-----键盘3 = 启动/停止宏
;卡药水：
;-----带吸血被动和杀回药水，换地图或死亡后先吃药水，再开翅膀，就卡上了
;##################################

;保证宏只在暗黑3中执行
#IfWinActive, ahk_class D3 Main Window Class
	Global MacroID := 0

	;恶魔猎手 - 暗影三刀
	;键盘3开启宏
	$3::
		If (MacroID = 0) {
			Gosub, 暗影之力
			DiabloIIIMacro(True, 3)
		} Else If (MacroID = 3) {
			DiabloIIIMacro(False, 0)
		}
	Return

	~A::
	{
		If (MacroID = 3) {
			SetTimer, 暗影之力, Off
			SetTimer, 暗影之力, On
		}
	}
	Return

	~S::
	{
		If (MacroID = 3) {
			SetTimer, 暗影之力, Off
			SetTimer, 暗影之力, On
		}
	}
	Return

	$D::
	{
		If (MacroID = 3) {
			While GetKeyState("D", "P") {
				Gosub, 暗影飞刀
				Sleep 50
			}
		} Else {
			SendInput {D}
		}
	}
	Return

	;卡药水
	~E::
	{
		If (MacroID = 3) {
			Sleep 100
			Gosub, 暗影之力
		}
	}
	Return

	;大地图、装备、聊天、快速回程等等停止宏，聊胜于无
	~B::
	~M::
	~Enter::
	~H::
		If (MacroID > 0) {
			DiabloIIIMacro(False, 0)
		}
	Return

	;技能设置：用户自定义修改技能按键配置
	强制移动:
		DiabloIIISendInput("{W}")
	Return

	左键:
		DiabloIIISendInput("{LButton}")
	Return

	右键:
		DiabloIIISendInput("{RButton}")
	Return

	YingLunFan:
		DiabloIIISendInput("{A}") ;影轮翻
	Return

	暗影之力:
		DiabloIIISendInput("{S}")
	Return

	暗影飞刀:
		DiabloIIISendInput("{D}")
	Return

	复仇:
		DiabloIIISendInput("{F}")
	Return

	战宠:
		DiabloIIISendInput("+{LButton}")
	Return

	DaoShan:
		DiabloIIISendInput("{RButton}") ;刀扇
	Return

#IfWinActive

;函数：保证按键和点击只在暗黑3中发送
DiabloIIISendInput(Keys) {
    If WinActive("ahk_class D3 Main Window Class") {
    	SendInput % Keys
    }
}

;函数：宏启动和停止
DiabloIIIMacro(StartMacro, params*) {
	SetTimer, 复仇, % (StartMacro ? 500 : "Off")
	SetTimer, DaoShan, % (StartMacro ? 500 : "Off")
	SetTimer, 战宠, % (StartMacro ? 1000 : "Off")
	SetTimer, 暗影之力, % (StartMacro ? 7500 : "Off")
	If (params[1] <> "") {
		MacroID := params[1]
	}
}
