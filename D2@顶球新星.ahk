﻿#SingleInstance force
#Persistent
#NoEnv

#IfWinActive, ahk_exe D2R.exe
	Global LeftPotionSlots := 3
	Global RightPotionSlots := 1
	Global CurrentLeftPotionSlot := 0
	Global CurrentRightPotionSlot := 0
	Global NovaCD := 0

	;CTA
	$Z::
	{
		CTABuff("R", "J|K|Z|X", 350, -160000) ;CTA+冰甲+顶球
	}
	Return

	;一键吨吨左侧N排药水
	$1::
	{
		If (CurrentLeftPotionSlot = 0) {
			CurrentLeftPotionSlot := 0 + LeftPotionSlots
		}
		SendInput % CurrentLeftPotionSlot
		CurrentLeftPotionSlot -= 1
	}
	Return

	;一键吨吨右侧N排药水
	$4::
	{
		If (CurrentRightPotionSlot = 0) {
			CurrentRightPotionSlot := 5 - RightPotionSlots
		}
		SendInput % CurrentRightPotionSlot
		CurrentRightPotionSlot += 1
		If (CurrentRightPotionSlot > 4) {
			CurrentRightPotionSlot := 0
		}
	}
	Return

	;一键入盒
	~E::
	{
		Sleep 150
		SendInput {LButton}
	}
	Return

	;辨识卷轴
	$V::
	{
		MouseGetPos, xpos, ypos
		MouseMove, 1340, 700, 0
		SendInput {RButton}
		Sleep 20
		MouseMove, xpos, ypos, 0
	}
	Return

	;FPS
	$=::
	{
		SendInput {Enter}
		Sleep 50
		SendInput {Raw}/fps
		Sleep 50
		SendInput {Enter}
	}
	Return

	;退出房间(1080P)
	$F1::
	{
		BlockInput ON
		SendInput {Esc}
		MouseMove, 960, 475, 0
		SendInput {LButton}
		BlockInput OFF
		SetTimer, CTACDBeep, OFF
	}
	Return

	;新建地狱房间(1080P)
	$F2::
	{
		BlockInput ON
		MouseMove, 800, 970, 0
		SendInput {LButton}
		Sleep 100
		MouseMove, 960, 585, 0
		SendInput {LButton}
		BlockInput OFF
		SetTimer, CTACDBeep, OFF
	}
	Return

	;小号进房A
	$F3::
	{
		BlockInput ON
		SendInput !{Tab}
		Sleep 200
		SendInput {Enter}
		Sleep 150
		MouseMove, 43, 860, 0
		SendInput {LButton}
		Sleep 150
		MouseMove, 226, 267, 0
		SendInput {LButton}
		Sleep 150
		MouseMove, 350, 300, 0
		SendInput {RButton}
		Sleep 200
		MouseMove, 450, 450, 0
		SendInput {LButton}
		Sleep 150
		SendInput !{Tab}
		BlockInput OFF
	}
	Return

	;小号进房B
	$F4::
	{
		BlockInput ON
		SendInput {Enter}
		Sleep 150
		MouseMove, 43, 860, 0
		SendInput {LButton}
		Sleep 150
		MouseMove, 226, 267, 0
		SendInput {LButton}
		Sleep 150
		MouseMove, 350, 300, 0
		SendInput {RButton}
		Sleep 200
		MouseMove, 450, 450, 0
		SendInput {LButton}
		BlockInput OFF
	}
	Return

	;小号进房C
	$F5::
	{
		BlockInput ON
		MouseMove, 1123, 968, 0
		SendInput {LButton}
		Sleep 500
		MouseMove, 183, 763, 0
		SendInput {LButton}
		Sleep 150
		MouseMove, 427, 150, 0
		SendInput {LButton}
		Sleep 150
		MouseMove, 516, 187, 0
		SendInput {RButton}
		Sleep 200
		MouseMove, 706, 361, 0
		SendInput {LButton}
		BlockInput OFF
	}
	Return

	CTACDBeep:
		ToolTip, `n     #战争召唤# BUFF结束!!!     `n `n, 580, 850
		SetTimer, CTACDToolTip, 3000
		SoundBeep, 300, 800
	Return

	CTACDToolTip:
		SetTimer, CTACDToolTip, OFF
		ToolTip
	return

#IfWinActive

;函数:CTABuff (CD提醒)
CTABUFF(CTA, Skills, Delay, CTACD) {
	SendInput % CTA
	Sleep 250
	Loop, Parse, % Skills, `|
	{
		SendInput % A_LoopField
		Sleep % Delay
	}
	Sleep 50
	SendInput % CTA
	SetTimer, CTACDBeep, OFF
	SetTimer, CTACDBeep, % CTACD
}
