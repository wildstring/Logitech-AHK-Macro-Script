#SingleInstance force
#Persistent
#NoEnv

;暗黑3/圣教军/阿克汉天谴/AHK脚本
;##################################
;说明：
;-----MacroID = 0代表宏已停止启动，MacroID = 1代表步兵宏已启动，MacroID = 2代表骑兵宏已启动
;-----步兵宏启动后，左键切换天谴延迟800ms，右键切换天谴延迟650ms
;-----骑兵宏启动后，左键停止一切技能上马跑，右键下马开启一切技能砍(同时左键连点拾取)
;-----宏启动后，按住Alt键左键连点拾取
;-----宏启动后，按住Ctrl键取消强制移动，人物原地站立
;-----宏启动后，按键盘H回城并停止宏，再启动按键盘1|2
;默认按键：
;-----鼠标左键 = 阿卡拉特勇士
;-----鼠标右键 = 天谴
;-----键盘A = 挑衅
;-----键盘S = 勇气律法
;-----键盘D = 主要技能/战马
;-----键盘F = 钢铁之肤
;-----键盘W = 强制移动
;-----键盘1 = 启动/停止步兵宏
;-----键盘2 = 启动/停止骑兵宏
;##################################

#IfWinActive, ahk_class D3 Main Window Class
	Global MacroID := 0

	$1::
		If (MacroID = 0) {
			DiabloIIIMacro(True, 1)
		} Else If (MacroID = 1) {
			DiabloIIIMacro(False, 0)
		}
	Return

	$2::
		If (MacroID = 0) {
			DiabloIIIMacro(True, 2)
		} Else If (MacroID = 2) {
			DiabloIIIMacro(False, 0)
		}
	Return

	~W::
		If (MacroID > 0) {
			While GetKeyState("W", "P") {
				Gosub, 左键
				Sleep 50
			}
		}
	Return

	;大地图、装备、聊天、快速回程等等停止宏，聊胜于无
	~Tab::
	~B::
	~H::
	~Enter::
	~Q::
		If (MacroID > 0) {
			DiabloIIIMacro(False, 0)
		}
	Return

	~Space::
		If (MacroID > 0) {
			While GetKeyState("Space", "P") {
				Gosub, 旋风斩关
				Sleep 50
			}
			Gosub, 旋风斩开
		}
	Return

	强制移动:
		DiabloIIISendInput("{W}")
	Return

	左键:
		DiabloIIISendInput("{LButton}")
	Return

	右键:
		DiabloIIISendInput("{RButton}")
	Return

	疾奔: ;战吼
		DiabloIIISendInput("{A}")
	Return

	痛割:
		DiabloIIISendInput("{S}")
	Return

	狂暴冲撞: ;大地践踏
		DiabloIIISendInput("{D}")
	Return

	狂暴者之怒I:
		DiabloIIISendInput("{F}")
	Return

	战斗怒火:
		DiabloIIISendInput("+{LButton}")
	Return

	旋风斩开:
		DiabloIIISendInput("{RButton Down}")
	Return

	旋风斩关:
		DiabloIIISendInput("{RButton Up}")
	Return

#IfWinActive

;函数：保证按键和点击只在暗黑3中发送
DiabloIIISendInput(Keys) {
    If WinActive("ahk_class D3 Main Window Class") {
    	SendInput % Keys
    }
}

;函数：宏启动和停止
DiabloIIIMacro(StartMacro, params*) {
	If (StartMacro) {
		Gosub, 疾奔 ;战吼
		Sleep 200
		Gosub, 战斗怒火
		Gosub, 狂暴冲撞 ;大地践踏
		Gosub, 旋风斩开
	} Else {
		Gosub, 旋风斩关
	}
	If (params[1] <> "") {
		MacroID := params[1]
	}
	SetTimer, 疾奔, % (StartMacro ? (MacroID = 1 ? 110000 : 4000) : "Off") ;战吼
	SetTimer, 战斗怒火, % (StartMacro ? 110000 : "Off")
	SetTimer, 狂暴者之怒I, % (StartMacro ? 250 : "Off")
}
